#en este archivo, vamos a recibir las peticiones del cliente (javascriṕt)

#primero, importamos los modulos que necesitemos 
from Flask import Flask, request 
from Flask_mysqldb import MySql

#segundo, instanciamos un objeto de la clase flask 
app = Flask (_ _name_ _)

#configurar las credenciales para acceder a la base de datos
app.config["MYSQL_user"]='root'
app.config["MYSQL_host"]='127.0.0.1'
app.config["MYSQL_password"]=''
app.config["MYSQL_db"]=''

#crear el objeto mysql para conectarse a la bd

mysql=MySql(app)