//Defino la clase Escena1 que hereda de Phaser.Scene
//Por lo tanto, Escena1 hereda atributos y métodos de Scene
class Escena1 extends Phaser.Scene {
    puntaje=0;
    item;
    texto;

    
    preload(){
         this.load.image("fondo","./accets/fondo.jpg");
         this.load.spritesheet("player","accets/sprite.png", {frameWidth:32, frameHeight:48});
         this.load.image("img1","accets/img1.png");
         this.load.image("img2","accets/img2.png");
         this.load.image("img3","accets/img3.png");
         this.load.image("img4","accets/img4.png");
         this.load.image("img5","accets/img5.png");
         this.load.image("img6","accets/img6.png");
         this.load.image("img7","accets/img7.png");
         this.load.image("img8","accets/img8.png");
         this.load.image("tacho-az","accets/tacho-az.png");
         this.load.image("tacho-am","accets/tacho-am.png");
         this.load.audio("musica-fondo","accets/audio/musica.mp3");

    }



    mostrarItem(){
        
        const aleatorio = Math.floor(Math.random() * (8-1) + 1);
        this.item = this.physics.add.sprite(500, 430, 'img' + aleatorio);
        this.item.setScale(0.1);
        let nombre = '';
        let categoria = "";
        switch (aleatorio) {
        case 1:
            nombre = 'papel';
            categoria = 'tacho-az';
            this.item.setScale(0.06);
        break;
        case 2:
            nombre = 'diario';
            categoria = 'tacho-az';
            this.item.setScale(0.09);
            break;
        case 3:
            nombre = 'hoja';
            categoria = 'tacho-az';
            this.item.setScale(0.04);
            break;
        case 4:
            nombre = 'pez';
            categoria = 'tacho-az';
            this.item.setScale(0.04);
            break;
        default:
        case 5:
            nombre = 'lata';
            categoria = 'tacho-am';
            this.item.setScale(0.09);
            break;
        case 6:
            nombre = 'caja';
            categoria = 'tacho-am';
            this.item.setScale(0.09);
            break;
        case 7:
            nombre = 'taza plastico';
            categoria = 'tacho-am';
            this.item.setScale(0.15);
            break;
        case 8:
            nombre = 'bolsa plastico';
            categoria = 'tacho-am';
            this.item.setScale(0.09);
            break;
 
        }
        this.item.setName(nombre);
        this.item.categoria = categoria;
        this.item.setCollideWorldBounds(true)
    } //fin mostrarItem


    create(){
        
     


        this.fondo = this.add.image(270,200, "fondo");
        this.fondo.setScale(1);
    
        this.player = this.physics.add.sprite(103,420,"player");
        this.player.setCollideWorldBounds(true);
    
        this.tacho_az = this.add.sprite(425,410,'tacho-az').setInteractive();
        this.tacho_az.setScale(0.07);

        this.tacho_az.on("pointerdown", ()=>{
             this.destruiritem("tacho-az");
             this.mostrarItem();
        
        this.sound.play("musica-fondo");
         })
    
         this.tacho_am = this.add.sprite(550,410,'tacho-am').setInteractive();
         this.tacho_am.setScale(0.07);

         this.tacho_am.on('pointerdown', ()=>{
             this.destruiritem("tacho-am");
             this.mostrarItem();
         
      
         })
       
         this.mostrarItem();

         const estilo = { font: "25px Arial", fill: "#FFFFFF"};
         this.texto = this.add.text(50, 50, "PUNTAJE: "+this.puntaje, estilo)
         this.texto.setStroke('#00f', 10);
         this.texto.setShadow(3,8, "#333333", 2);

    
         this.anims.create({
            key: 'izquierda',
            frames: this.anims.generateFrameNumbers('player', { start: 0, end: 3 }),
            frameRate: 10,
            repeat: -1 //ejecuta infinitamente
            });
    
        this.anims.create({
            key: 'derecha',
            frames: this.anims.generateFrameNumbers('player', { start: 5, end: 8 }),
            frameRate: 10,
            repeat: -1 //ejecuta infinitamente
            });
    
    
        this.anims.create({
            key: 'quieto',
            frames: this.anims.generateFrameNumbers('player', { start: 4, end: 4 }),
            frameRate: 10,
            repeat: -1 //ejecuta infinitamente
            });
    
    
        this.teclas = this.input.keyboard.createCursorKeys();
    
          
        }
    
        update(){
    
            if (this.teclas.right.isDown) {
                this.player.x += 3;
                this.player.anims.play("derecha", true);
                this.item.x -= 3;
            }
    
            if (this.teclas.left.isDown) {
                this.player.x += -3;
                this.player.anims.play("izquierda", true);
                this.item.x += 3;
            }
            if (!this.teclas.left.isDown && !this.teclas.right.isDown) {
                this.player.x += 0;
                this.player.anims.play("quieto", true);
            }
    
            this.texto.setText("PUNTAJE: " + this.puntaje);
        }

      
        destruiritem(categoria){
            console.log("categoria item: ", this.item.categoria);
            console.log("categoria tacho: ", categoria);


            if (this.item.categoria == categoria) {
                this.puntaje += 1;
                this.item.destroy();
               
            }
            else {
                console.log(" no coincide");
                this.puntaje -= 1;
                this.item.destroy();
            }
        }
    
    
    }
    
    
    const config = {
    
       type: Phaser.AUTO,
        width: 640,
        height: 450,
        parent: 'juego',
        scene: [Escena1],
        physics: {
            default: 'arcade',
            arcade: {
                debug: false,
                gravity: {
                    y: 200
                },
            },
        },
    };
    
    new Phaser.Game(config);
